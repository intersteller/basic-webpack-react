import { resolve } from 'path'
import webpack from 'webpack'
import ExtractTextPlugin from 'extract-text-webpack-plugin'

module.exports = [
  /* Webpack client configuration */
  {
    name: 'client',
    target: 'web',
    devtool: 'cheap-module-source-map',
    entry: [
      'react-hot-loader/patch',
      'webpack-hot-middleware/client',
      './src/client'
    ],
    output: {
      filename: 'client.js',
      path: resolve(__dirname, 'static'),
      publicPath: '/',
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          loaders: ['babel-loader'],
          exclude: /node_modules/
        },
        {
          test: /\.(sass|scss)$/,
          loaders: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'],
          exclude: /node_modules/
        },
        {
          test: /\.(gif|png|jpe?g|svg)$/i,
          use: [
            'url-loader?limit=10000',
            'img-loader'
          ]
        },
        { test: /\.eot(\?v=\d+.\d+.\d+)?$/, use: 'file-loader' },
        { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, use: 'url-loader?limit=10000&mimetype=application/font-woff' },
        { test: /\.[ot]tf(\?v=\d+.\d+.\d+)?$/, use: 'url-loader?limit=10000&mimetype=application/octet-stream' },
        { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, use: 'url-loader?limit=10000&mimetype=image/svg+xml' }
      ]
    },
    resolve: {
      extensions: ['.js', '.jsx', '.scss']
    },
    plugins: [
      new webpack.NamedModulesPlugin(),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoEmitOnErrorsPlugin()
    ]
  },

  /* Webpack server conguration */
  {
    name: 'server',
    target: 'node',
    entry: [
      './src/server'
    ],
    output: {
      path: resolve(__dirname, 'static'),
      filename: 'server.js',
      libraryTarget: 'commonjs2',
      publicPath: '/static/',
    },
    devtool: 'cheap-module-source-map',
    resolve: {
      extensions: ['.js', '.jsx']
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          loaders: ['babel-loader'],
          exclude: /node_modules/
        },
        {
          test: /\.scss$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
              {
                loader: 'css-loader',
                options: {
                  url: false,
                  minimize: true,
                  sourceMap: true
                }
              },
              {
                loader: 'sass-loader',
                options: {
                  sourceMap: true
                }
              }
            ]
          })
        }
      ],
    },
    plugins: [
      new ExtractTextPlugin('styles.css'),
      new webpack.NamedModulesPlugin(),
      new webpack.NoEmitOnErrorsPlugin()
    ]
  }
]
