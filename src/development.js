import express from 'express'
import webpack from 'webpack'
import webpackDevMiddleware from 'webpack-dev-middleware'
import webpackHotMiddleware from 'webpack-hot-middleware'
import webpackHotServerMiddleware from 'webpack-hot-server-middleware'
import config from './webpack.development.config.js'

import { getPlanet, getPeople } from './services/Api'
const app = express()
const router = express.Router()
const compiler = webpack(config)

app.use(webpackDevMiddleware(compiler, {
  noInfo: false,
  publicPath: '/'
}))

app.use(webpackHotMiddleware(compiler.compilers.find(compiler => compiler.name === 'client'), {
  'log': false,
  'path': '/__webpack_hmr',
  'heartbeat': 20000,
  'reload': true
}))

/* API routes config */
router.use(function (req, res, next) {
  next()
})

router.get('/planets', function (req, res) {
  const data = getPlanet()
  data.then(data => res.json(data))
})

router.get('/people/:id', function (req, res) {
  const id = req.params.id
  const data = getPeople(id)
  data.then(data => res.json(data))
})
/* end API routes config */

app.use('/api', router)

app.use(webpackHotServerMiddleware(compiler))
app.listen(8000)
