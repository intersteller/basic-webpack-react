import axios from 'axios'

const swapiUrl = 'https://swapi.co/api/'

export const getPlanet = () => {
  const apiUrl = swapiUrl + 'planets/'

  return axios.get(apiUrl)
    .then(function (response) {
      return response.data
    })
    .catch(function (error) {
      console.log(error)
    })
}

export const getPeople = (id) => {
  const apiUrl = swapiUrl + 'people/' + id

  return axios.get(apiUrl)
    .then(function (response) {
      return response.data
    })
}
