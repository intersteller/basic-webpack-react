import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import { Helmet } from 'react-helmet'

import './assets/scss/styles.scss'
import Nav from './components/Navigation'
import Homepage from './components/Homepage'

class App extends Component {
  render () {
    return (
      <div>
        <Helmet
          htmlAttributes={{ lang: 'en', amp: undefined }}
          titleTemplate='%s | Basic React'
          titleAttributes={{ itemprop: 'name', lang: 'en' }}
          meta={[
            { name: 'description', content: 'NGOAR demo' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
          ]}
        />

        <div className='flexbox'>
          <Nav />

          <section id='main_page'>
            <Switch>
              <Route exact path='/' component={Homepage} />
            </Switch>
          </section>
        </div>
      </div>
    )
  }
}

export default App
