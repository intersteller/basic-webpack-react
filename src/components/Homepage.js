import React, { Component } from 'react'
import { Helmet } from 'react-helmet'
import axios from 'axios'

import Planet from './Planet'

class Homepage extends Component {
  constructor (props) {
    super(props)

    this.state = {
      data: null
    }
  }

  componentDidMount () {
    const data = axios.get('http://localhost:8000/api/planets/')
    data.then(res => this.setState({ data: res.data }))
  }

  render () {
    const { data } = this.state

    let planetContent = <div>Loading...</div>
    if (data !== null) {
      const planetData = data.results
      planetContent = planetData.map((planet, key) => <Planet key={key} planet={planet} />)
    }

    return (
      <section id='main_page'>
        <div className='main_content col'>
          <Helmet title='Welcome to Homepage' />
          <h1>Homepage</h1>
          { planetContent }
        </div>
      </section>
    )
  }
}

export default Homepage
