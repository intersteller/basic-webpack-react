import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import {
  SettingsIcon,
  HomeIcon,
  FileDocumentIcon,
  BellRingOutlineIcon,
  HelpCircleIcon,
  AccountCircleIcon,
  ArrowRightBoldBoxOutlineIcon
} from 'react-material-icon-svg'
import '../assets/scss/navigation.scss'

class Menu extends Component {
  render () {
    return (
      <section id='navigation'>
        <div className='left_menu col'>
          <ul className='top-menu'>
            <li><Link to={'/'}><HomeIcon /> <span>Home</span></Link></li>
            <li><Link to={'/items'}><FileDocumentIcon /> <span>Items</span></Link></li>
          </ul>
          <ul className='bottom_menu'>
            <li className='alert'>
              <Link to={'/alert'}><BellRingOutlineIcon /> <span>Alert</span></Link>
              <span className='num'>1</span>
            </li>
            <li><Link to={'/setting'}><SettingsIcon /> <span>Setting</span></Link></li>
            <li><Link to={'/help'}><HelpCircleIcon /> <span>Help</span></Link></li>
            <li><Link to={'/profiles'}><AccountCircleIcon /> <span>Profile</span></Link></li>
            <li><Link to={'/profiles'}><ArrowRightBoldBoxOutlineIcon /> <span>Logout</span></Link></li>
          </ul>
        </div>
      </section>
    )
  }
}

export default Menu
