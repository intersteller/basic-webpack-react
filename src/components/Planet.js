import React, { Component } from 'react'
import PropsTypes from 'prop-types'

class Planet extends Component {
  render () {
    const { planet } = this.props

    return (
      <div>{planet.name}</div>
    )
  }
}

Planet.propTypes = {
  planet: PropsTypes.object.isRequired
}

export default Planet
