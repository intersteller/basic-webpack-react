import React from 'react'
import { renderToString } from 'react-dom/server'
import { StaticRouter } from 'react-router-dom'
import { Helmet } from 'react-helmet'

import './assets/scss/styles.scss'
import Template from './layouts/template'
import App from './App'

const serverRenderer = ({ clientStats, serverStats }) => {
  return (req, res, next) => {
    const context = {}
    const markup = renderToString(
      <StaticRouter location={req.url} context={context}>
        <App />
      </StaticRouter>
    )
    const helmet = Helmet.renderStatic()

    res.status(200).send(Template({
      markup: markup,
      helmet: helmet
    }))
  }
}

export default serverRenderer
