import React from 'react'
import { hydrate } from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { AppContainer } from 'react-hot-loader'

import App from './App'

const renderContainer = Component => {
  const root = document.getElementById('root')

  if (root) {
    hydrate((
      <AppContainer>
        <BrowserRouter>
          <Component />
        </BrowserRouter>
      </AppContainer>
    ), root)
  }
}

renderContainer(App)

if (module.hot) {
  module.hot.accept('./App', () => { renderContainer(App) })
}
